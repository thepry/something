# frozen_string_literal: true

require 'rspec'
require_relative '../../lib/services/customers_within_distance_from_coordinates'
require_relative '../../lib/services/parse_customers'

RSpec.describe CustomersWithinDistanceFromCoordinates do
  let(:customers_text) do
    '{"latitude": "52.986375", "user_id": 12, "name": "Christina McArdle", "longitude": "-6.043701"}
     {"latitude": "51.92893", "user_id": 1, "name": "Alice Cahill", "longitude": "-10.27699"}
     {"latitude": "51.92893", "name": "Alice Cahill", "longitude": "-10.27699"}'
  end

  let(:customers) do
    customers_data = ParseCustomers.call(text: customers_text)

    customers_data.map do |customer_data|
      Customer.new(
        user_id: customer_data['user_id'],
        name: customer_data['name'],
        longitude: customer_data['longitude'],
        latitude: customer_data['latitude']
      )
    end
  end

  let(:dublin_latitude) { 53.3393 }
  let(:dublin_longitude) { -6.2576841 }

  let(:default_params) do
    {
      customers: customers,
      distance_km: 200,
      latitude: dublin_latitude,
      longitude: dublin_longitude
    }
  end

  it 'returns customers with distance' do
    customers_within_200km = described_class.call(**default_params)

    expect(customers_within_200km.size).to eq 1
    expect(customers_within_200km.first.name).to eq 'Christina McArdle'

    customers_within_2000km = described_class.call(
      **default_params.merge(distance_km: 2000)
    )

    expect(customers_within_2000km.size).to eq 3
  end

  it 'raises exception if arguments types are wrong' do
    default_params.each do |key, _value|
      new_params = default_params.merge(key => nil)
      expect { described_class.call(**new_params) }.to raise_error(ArgumentError)
    end
  end
end
