# frozen_string_literal: true

require 'rspec'
require_relative '../../lib/services/calculate_distance_from_coordinates'
require_relative '../../lib/services/parse_customers'

RSpec.describe CalculateDistanceFromCoordinates do
  let(:correct_coordinates) do
    {
      latitude1: 52.520008, # Berlin
      longitude1: 13.404954,
      latitude2: 40.730610, # New York
      longitude2: -73.935242
    }
  end

  let(:berlin_nyc_distance) { 6396.75 }

  it 'calcucaltes correct distance' do
    distance = described_class.call(**correct_coordinates)
    expect(distance).to be_within(berlin_nyc_distance * 0.005).of(berlin_nyc_distance)
  end

  it 'raises exception if wrong coordinates_types are passed' do
    correct_coordinates.each do |key, _|
      new_coordinates = correct_coordinates.merge(key => '15')
      expect { described_class.call(**new_coordinates) }.to raise_error(ArgumentError)
    end
  end
end
