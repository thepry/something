# frozen_string_literal: true

require 'rspec'
require_relative '../../lib/services/flatten_array'

RSpec.describe FlattenArray do
  it 'raises exception if nil is passed' do
    expect { described_class.call(nil) }.to raise_error(ArgumentError)
  end

  it 'raises exception if not array is passed' do
    expect { described_class.call(a: :b) }.to raise_error(ArgumentError)
    expect { described_class.call(1) }.to raise_error(ArgumentError)
  end

  it 'flattens flat array' do
    result = described_class.call([1, 2, 3, 4, 5, 6, 7, 8])
    expect(result).to eq [1, 2, 3, 4, 5, 6, 7, 8]
  end

  it 'flattens [1, [2, [3, 4], [5]], 6, [7, 8]]' do
    result = described_class.call([1, [2, [3, 4], [5]], 6, [7, 8]])
    expect(result).to eq [1, 2, 3, 4, 5, 6, 7, 8]
  end

  it 'flattens [1, [2, 3] 4, 5], [6], 7, 8]' do
    result = described_class.call([1, [2, 3], 4, 5, [6], 7, 8])
    expect(result).to eq [1, 2, 3, 4, 5, 6, 7, 8]
  end

  it 'flattens [1, [2, [3 [4, [5, [6, [7, [8]]]]]]]]' do
    result = described_class.call([1, [2, [3, [4, [5, [6, [7, [8]]]]]]]])
    expect(result).to eq [1, 2, 3, 4, 5, 6, 7, 8]
  end

  it 'flattens [[[[[[[[1], 2], 3], 4], 5], 6], 7], 8]' do
    result = described_class.call([[[[[[[[1], 2], 3], 4], 5], 6], 7], 8])
    expect(result).to eq [1, 2, 3, 4, 5, 6, 7, 8]
  end
end
