# frozen_string_literal: true

require 'rspec'
require_relative '../../lib/services/customers_within_distance_from_city'
require_relative '../../lib/services/parse_customers'
require_relative '../../lib/models/city'

RSpec.describe CustomersWithinDistanceFromCity do
  let(:dublin) do
    City.new(
      name: 'Dublin',
      latitude: 53.3393,
      longitude: -6.2576841
    )
  end

  let(:customers) do
    path = File.join(File.dirname(__FILE__), '../../', 'customers.json')
    customers_data = ParseCustomers.call(text: File.read(path))

    customers_data.map do |customer_data|
      Customer.new(
        user_id: customer_data['user_id'],
        name: customer_data['name'],
        longitude: customer_data['longitude'],
        latitude: customer_data['latitude']
      )
    end
  end

  it 'returns sorted customers list within distance' do
    result = described_class.call(city: dublin, customers: customers, distance_km: 100)
    expect(result.map(&:user_id).sort).to eq [4, 5, 6, 8, 11, 12, 13, 15, 17, 23, 24, 26, 29, 30, 31, 39]
  end
end
