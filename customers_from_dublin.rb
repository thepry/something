# frozen_string_literal: true

require_relative './lib/models/city'
require_relative './lib/services/customers_within_distance_from_city'
require_relative './lib/services/parse_customers'

dublin = City.new(name: 'Dublin', latitude: 53.3393, longitude: -6.2576841)
path = File.join(File.dirname(__FILE__), 'customers.json')

customers_data = ParseCustomers.call(text: File.read(path))

customers = customers_data.map do |customer_data|
  Customer.new(
    user_id: customer_data['user_id'],
    name: customer_data['name'],
    longitude: customer_data['longitude'],
    latitude: customer_data['latitude']
  )
end

customers_within_distance = CustomersWithinDistanceFromCity.call(city: dublin, distance_km: 100, customers: customers)

customers_within_distance.sort_by(&:user_id).each do |customer|
  puts "Customer: #{customer.name}, user_id: #{customer.user_id}"
end
