# frozen_string_literal: true

module FlattenArray
  class << self
    def call(array)
      check_type(array)
      flatten(array: array)
    end

    private

    def check_type(argument)
      raise(ArgumentError, "Argument must be an Array type, not #{argument.class}") unless argument.is_a? Array
    end

    def flatten(array:, accumulator: [])
      array.each do |element|
        next accumulator << element unless element.is_a? Array

        flatten(array: element, accumulator: accumulator)
      end

      accumulator
    end
  end
end
