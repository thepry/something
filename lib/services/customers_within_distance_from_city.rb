# frozen_string_literal: true

require_relative './customers_within_distance_from_coordinates'

module CustomersWithinDistanceFromCity
  class << self
    def call(city:, distance_km:, customers:)
      CustomersWithinDistanceFromCoordinates.call(
        customers: customers,
        distance_km: distance_km,
        latitude: city.latitude,
        longitude: city.longitude
      )
    end
  end
end
