# frozen_string_literal: true

require 'json'
require_relative '../models/customer'

module ParseCustomers
  class << self
    def call(text:)
      text.split("\n").map do |json_line|
        JSON.parse(json_line)
      end
    end
  end
end
