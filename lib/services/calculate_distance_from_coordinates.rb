# frozen_string_literal: true

# Calculates distance between two points using Haversine formula
# Assumes that Earth is sphere. The result might be incorect for 0.5%

module CalculateDistanceFromCoordinates
  RAD_PER_DEGREE = Math::PI / 180 # PI / 180
  EARTH_RADIUS = 6371 # Earth radius in kilometers

  class << self
    def call(latitude1:, longitude1:, latitude2:, longitude2:)
      check_types(latitude1: latitude1, longitude1: longitude1, latitude2: latitude2, longitude2: longitude2)
      distance_delta = delta(
        latitude1: latitude1,
        longitude1: longitude1,
        latitude2: latitude2,
        longitude2: longitude2
      )

      distance_delta * EARTH_RADIUS # delta in kilometers
    end

    private

    def check_types(params)
      params.each do |argument, value|
        unless value.is_a? Numeric
          raise(ArgumentError,
                "Arguments must be numberic. #{argument} has a type #{value.class}")
        end
      end
    end

    def delta(latitude1:, longitude1:, latitude2:, longitude2:)
      delta_latitude = coordinates_delta(coord1: latitude1, coord2: latitude2)
      delta_longitude = coordinates_delta(coord1: longitude1, coord2: longitude2)
      lat1_rad = rad(coord: latitude1)
      lat2_rad = rad(coord: latitude2)

      apply_formula(
        delta_latitude: delta_latitude,
        delta_longitude: delta_longitude,
        lat1_rad: lat1_rad,
        lat2_rad: lat2_rad
      )
    end

    def apply_formula(delta_latitude:, delta_longitude:, lat1_rad:, lat2_rad:)
      sqr_sin = Math.sin(delta_latitude / 2)**2
      result = sqr_sin + Math.cos(lat1_rad) * Math.cos(lat2_rad) * Math.sin(delta_longitude / 2)**2

      2 * Math.atan2(Math.sqrt(result), Math.sqrt(1 - result))
    end

    def rad(coord:)
      coord * RAD_PER_DEGREE
    end

    def coordinates_delta(coord1:, coord2:)
      (coord2 - coord1) * RAD_PER_DEGREE # Delta, converted to rad
    end
  end
end
