# frozen_string_literal: true

require_relative './calculate_distance_from_coordinates'

module CustomersWithinDistanceFromCoordinates
  class << self
    def call(customers:, distance_km:, longitude:, latitude:)
      return unless check_types(customers: customers,
                                distance_km: distance_km,
                                longitude: longitude,
                                latitude: latitude)

      customers_within_distance(customers: customers,
                                distance_km: distance_km,
                                longitude: longitude,
                                latitude: latitude)
    end

    private

    def customers_within_distance(customers:, distance_km:, longitude:, latitude:)
      customers.select do |customer|
        next false if customer.longitude.nil? || customer.latitude.nil?

        customer_distance = CalculateDistanceFromCoordinates.call(
          longitude1: longitude,
          latitude1: latitude,
          longitude2: customer.longitude,
          latitude2: customer.latitude
        )

        customer_distance <= distance_km
      end
    end

    def check_types(customers:, distance_km:, longitude:, latitude:)
      unless customers.is_a? Array
        raise(ArgumentError,
              "Argument customers must be an Array type, not #{customers.class}")
      end

      unless distance_km.is_a? Numeric
        raise(ArgumentError,
              "Argument distance_km must be an Numeric type, not #{distance_km.class}")
      end

      unless longitude.is_a? Numeric
        raise(ArgumentError,
              "Argument longitude must be an Numeric type, not #{longitude.class}")
      end

      unless latitude.is_a? Numeric
        raise(ArgumentError,
              "Argument latitude must be an Numeric type, not #{latitude.class}")
      end

      true
    end
  end
end
