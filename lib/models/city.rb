# frozen_string_literal: true

class City
  attr_reader :name,
              :longitude,
              :latitude

  def initialize(name:, longitude:, latitude:)
    @name = name
    @longitude = longitude.to_f
    @latitude = latitude.to_f
  end
end
