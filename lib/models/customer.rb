# frozen_string_literal: true

class Customer
  attr_reader :user_id,
              :name,
              :longitude,
              :latitude

  def initialize(user_id:, name:, longitude:, latitude:)
    @user_id = user_id
    @name = name
    @longitude = longitude.to_f
    @latitude = latitude.to_f
  end
end
